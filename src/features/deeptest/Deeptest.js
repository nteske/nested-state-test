import React from 'react';

import { useSelector,useDispatch,shallowEqual } from 'react-redux';
import { selectCommentName, setNewDate,setNewName } from './deeptestSlice';

export function Deeptest() {
    const dispatch = useDispatch();
    const commentName = useSelector((state)=>selectCommentName(state,{
      workspace:11,
      baord:4,
      item:9,
      comment:102
    }),shallowEqual);
    
  console.log(commentName);
  return (
    <div>
      <button onClick={()=>dispatch(setNewDate())}>set new date</button>
      <button onClick={()=>dispatch(setNewName())}>set new name</button>
    </div>
  );
}
