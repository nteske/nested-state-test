import { createSlice, createSelector } from "@reduxjs/toolkit";
import { nestedReducer } from "./nested/nestedReducer";

const initialState = {
  id:1,
  name:"test",
  workspaces:{
    11:{
      name:"my workspace",
      boards:{
        4:{
          name:"my board",
          items:{
            9:{
              name:"my item",
              comments:{
                102:{
                  id: 3,
                  name:"my comment",
                  date: (new Date()).toISOString()
                }
              }
            }
          }
        }
      }
    }
  }
};

export const deeptestSlice = createSlice({
  name: "deeptest",
  initialState,
  reducers: {
    ...nestedReducer
  },
});

//state.workspaces[11].boards[4].items[9].comments[102].date=(new Date()).toISOString();
export const selectCommentName = createSelector(
  state=>state.deeptest.workspaces,
  (state,data)=>data,
  (workspaces,data) => {
    return {
      name:workspaces[data.workspace].boards[data.baord].items[data.item].comments[data.comment].name
    };
  }
); 




export const { setNewDate,setNewName } = deeptestSlice.actions;

export default deeptestSlice.reducer;
