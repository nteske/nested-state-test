import React from 'react';
import { useDispatch } from 'react-redux';
import {
  laodBigOne
} from './bigoneSlice';

export function Bigone() {
  const dispatch = useDispatch();

  return (
    <div>
       <button onClick={()=>dispatch(laodBigOne())}>Load big ammount of data</button>
    </div>
  );
}
