import React from 'react';
import {
  fetchOrganization, fetchWorkspace,selectItemWithCommentsByIds, selectOrganization, updateWorskace
} from './organizationSlice';
import { useSelector,useDispatch } from 'react-redux';

export function Organization() {
    const dispatch = useDispatch();
    const workspace = useSelector((state)=>selectItemWithCommentsByIds(state,10));
    const organization = useSelector(selectOrganization);
    console.log("organization",organization)

    console.log("test",workspace)
  return (
    <div>
        <button onClick={()=>dispatch(fetchOrganization())}>Fetch Organization</button>
        <button onClick={()=>dispatch(fetchWorkspace())}>Fetch Workspace</button>
        <button onClick={()=>dispatch(updateWorskace())}>Update Workspace</button>
        <p>{JSON.stringify(workspace,undefined,4)}</p>
        <p>Organizacija {organization.name}</p>
    </div>
  );
}
