import { createSlice, createSelector } from "@reduxjs/toolkit";
import { organizationReducers } from "./organizationReducers";
import { workspaceReducers } from "./workspaces/workspaceReducers";

const initialState = {
  id: 0,
  name: "",
  slug: "",
  workspaces: {},
};

export const organizationSlice = createSlice({
  name: "organization",
  initialState,
  reducers: {
    ...organizationReducers,
    ...workspaceReducers,
  },
});

export const { fetchOrganization, fetchWorkspace, updateWorskace } =
  organizationSlice.actions;

/* export const selectOrganization = createSelector(
  [({ organization }) => organization.id,
  ({ organization }) => organization.name,
  ({ organization }) => organization.slug],
  (id, name, slug) => ({ id, name, slug })
); */

export const selectOrganization = createSelector(
  state=>state.organization.id,
  state=>state.organization.name,
  state=>state.organization.slug,
  (id,name,slug) => ( {id,name,slug })
); 

export const selectWorkspaces = (state) => state.organization.workspaces;
const selectItemById = (state, id) => id;

export const selectItemWithCommentsByIds = createSelector(
  [selectWorkspaces, selectItemById],
  (workspaces, id) => {
    return workspaces[id];
  }
);

export default organizationSlice.reducer;
