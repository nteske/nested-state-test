import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import deeptestReducer from '../features/deeptest/deeptestSlice';
import bigoneReducer from '../features/bigOne/bigoneSlice';

import organizationReducer from '../features/organization/organizationSlice';
import { combineReducers } from 'redux'

import {
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist'
import localForage from 'localforage';


const persistConfig = {
  key: 'root',
  version: 1,
  storage: localForage
}

const rootReducer = combineReducers({
  counter: counterReducer,
  organization: organizationReducer,
  deeptest: deeptestReducer,
  bigone:bigoneReducer
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
  getDefaultMiddleware({
    immutableCheck: false,
    serializableCheck: false,
  }),
});
