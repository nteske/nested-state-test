import React from 'react';
import logo from './logo.svg';
import { Organization } from './features/organization/Organization';
import {Deeptest} from './features/deeptest/Deeptest';
import { Bigone } from './features/bigOne/Bigone';

import { getRandomInt } from './utils/random';
import './App.css';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Bigone></Bigone>
        <img src={logo} className="App-logo" alt="logo" />
        <Organization />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <span>
          <span>Learn </span>
          <a
            className="App-link"
            href="https://reactjs.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React
          </a>
          <span>, </span>
          <a
            className="App-link"
            href="https://redux.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Redux
          </a>
          <span>, </span>
          <a
            className="App-link"
            href="https://redux-toolkit.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Redux Toolkit
          </a>
          ,<span> and </span>
          <a
            className="App-link"
            href="https://react-redux.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React Redux
          </a>
        </span>
        <button onClick={()=>{console.log("random",getRandomInt())}}>Random Number in Console No State Changes</button>
        <p>Deep test</p>
        <Deeptest></Deeptest>
      </header>
    </div>
  );
}

export default App;
